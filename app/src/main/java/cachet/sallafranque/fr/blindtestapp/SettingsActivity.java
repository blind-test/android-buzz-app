package cachet.sallafranque.fr.blindtestapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

import static android.support.design.widget.Snackbar.LENGTH_SHORT;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        SharedPreferences sharedPref = getSharedPreferences(
                Constants.APP_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String savedIpAddress = sharedPref.getString(Constants.IP_ADDRESS_KEY, null);

        EditText editText = (EditText) findViewById(R.id.ipAddress);
        editText.setText(savedIpAddress);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void validate(View view){
        EditText editText = (EditText) findViewById(R.id.ipAddress);
        String ipAddress = editText.getText().toString();

        // Sauvegarde du groupe en local
        SharedPreferences sharedPref = getSharedPreferences(
                Constants.APP_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constants.IP_ADDRESS_KEY, ipAddress);
        editor.apply();

        Snackbar confirmMessage = Snackbar.make(view, "IP enregistrée", LENGTH_SHORT);
        confirmMessage.show();
    }
}

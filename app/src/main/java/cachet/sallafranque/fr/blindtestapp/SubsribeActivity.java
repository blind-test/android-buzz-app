package cachet.sallafranque.fr.blindtestapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import cachet.sallafranque.fr.blindtestapp.network.NetworkConnector;

import static android.support.design.widget.Snackbar.LENGTH_SHORT;

public class SubsribeActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "groupName.message";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Recuperation du nom du groupe
        SharedPreferences sharedPref = getSharedPreferences(
                Constants.APP_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String savedGroupName = sharedPref.getString(Constants.GROUP_NAME_KEY, null);
        if(savedGroupName == null){
            // Affichage de la page de creation de groupe
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_subsribe);
            Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(myToolbar);
        }else{
            // Naviguation vers la page buzzer
            Intent intent = new Intent(this, BuzzerActivity.class);
            intent.putExtra(EXTRA_MESSAGE, savedGroupName);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        SharedPreferences sharedPref = getSharedPreferences(
                Constants.APP_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String savedGroupName = sharedPref.getString(Constants.GROUP_NAME_KEY, null);
        if(savedGroupName == null){
            super.onResume();
        }else{
            // Naviguation vers la page buzzer
            Intent intent = new Intent(this, BuzzerActivity.class);
            intent.putExtra(EXTRA_MESSAGE, savedGroupName);
            startActivity(intent);
        }
    }

    public void createGroup(final View view){
        // Recuperation du nom du groupe
        final EditText editText = (EditText) findViewById(R.id.groupName);
        String groupName = editText.getText().toString();

        SharedPreferences sharedPref = getSharedPreferences(
                Constants.APP_PREFERENCES_NAME, Context.MODE_PRIVATE);

        // Sauvegarde du groupe sur le serveur
        NetworkConnector.getInstance(getApplicationContext()).createGroup(
                groupName,
                sharedPref.getString(Constants.IP_ADDRESS_KEY, null),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // Sauvegarde du groupe en local
                        String id = null;
                        String name = null;
                        try {
                            id = response.getString("id");
                            name = response.getString("name");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        SharedPreferences sharedPref = getSharedPreferences(
                                Constants.APP_PREFERENCES_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(Constants.GROUP_ID_KEY, id);
                        editor.putString(Constants.GROUP_NAME_KEY, name);
                        editor.apply();

                        // Naviguation vers la page buzzer
                        Intent intent = new Intent(SubsribeActivity.this, BuzzerActivity.class);
                        intent.putExtra(EXTRA_MESSAGE, name);
                        SubsribeActivity.this.startActivity(intent);
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Snackbar confirmMessage = Snackbar.make(view, error.getMessage(), LENGTH_SHORT);
                        confirmMessage.show();
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                // Naviguation vers la page buzzer
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}

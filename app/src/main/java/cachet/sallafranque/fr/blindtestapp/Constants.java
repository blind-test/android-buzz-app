package cachet.sallafranque.fr.blindtestapp;

/**
 * Created by msi on 01/07/2017.
 */

public class Constants {


    public static final String APP_PREFERENCES_NAME = "blindTestAppPreferences";

    public static final String GROUP_NAME_KEY = "groupName";

    public static final String IP_ADDRESS_KEY = "ipAddress";

    public static final String GROUP_ID_KEY = "groupId";
}

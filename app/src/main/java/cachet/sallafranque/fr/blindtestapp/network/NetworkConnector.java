package cachet.sallafranque.fr.blindtestapp.network;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Singleton pour les connexions réseau.
 */

public class NetworkConnector {
    private static NetworkConnector mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    private NetworkConnector(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized NetworkConnector getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NetworkConnector(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public void createGroup(String groupName, String ipAddress, Listener<JSONObject> responseListener, ErrorListener errorListener){
        JSONObject object = new JSONObject();
        try {
            object.put("name", groupName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                "http://" + ipAddress + ":60899/api/group",
                object,
                responseListener,
                errorListener);

        addToRequestQueue(request);
    }

    public void sendBuzz(String groupId, String ipAddress, Listener<JSONObject> responseListener, ErrorListener errorListener){
        JSONObject object = new JSONObject();
        try {
            object.put("id", groupId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                "http://" + ipAddress + ":60899/api/test",
                object,
                responseListener,
                errorListener);

        addToRequestQueue(request);
    }
}

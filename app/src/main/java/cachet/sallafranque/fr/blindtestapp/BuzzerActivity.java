package cachet.sallafranque.fr.blindtestapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import cachet.sallafranque.fr.blindtestapp.network.NetworkConnector;

import static android.support.design.widget.Snackbar.LENGTH_SHORT;

public class BuzzerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buzzer);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        Intent intent = getIntent();
        String groupName = intent.getStringExtra(SubsribeActivity.EXTRA_MESSAGE);

        TextView textView = (TextView) findViewById(R.id.groupName);
        textView.setText(groupName);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void buzz(final View view){
        SharedPreferences sharedPref = getSharedPreferences(
                Constants.APP_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String groupId = sharedPref.getString(Constants.GROUP_ID_KEY, null);

        NetworkConnector.getInstance(getApplicationContext()).sendBuzz(
                groupId,
                sharedPref.getString(Constants.IP_ADDRESS_KEY, null),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // message de confirmation
                        Snackbar confirmMessage = Snackbar.make(view, "Buzz pris en compte", LENGTH_SHORT);
                        confirmMessage.show();
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.getCause() instanceof JSONException){
                            Snackbar confirmMessage = Snackbar.make(view, "Buzz pris en compte", LENGTH_SHORT);
                            confirmMessage.show();
                        }else{
                            Snackbar confirmMessage = Snackbar.make(view, error.getMessage(), LENGTH_SHORT);
                            confirmMessage.show();
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                // Naviguation vers la page buzzer
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
